#![deny(missing_docs)]
/*!
See [README.md](https://crates.io/crates/sylasteven-system-input-default)
**/

use sylasteven as engine;

use glutin::{EventsLoop, GlContext, GlWindow};

pub use glutin::{ModifiersState as Modifiers, MouseButton, VirtualKeyCode as KeyCode};

/// An event representing input.
pub enum InputEvent {
    /// The event when the window is resized.
    Resize {
        /// The new window width.
        width: u32,
        /// The new window height.
        height: u32,
    },
    /// The event when a key is pressed or released.
    Key {
        /// The code of the pressed or released key.
        keycode: KeyCode,
        /// The pressed modifiers.
        modifiers: Modifiers,
        /// A flag if the key is pressed, not released.
        down: bool,
    },
    /// The event for text input.
    TextInput(char),
    /// The event when a mouse button is clicked or released.
    MouseClick {
        /// The clicked or released button.
        button: MouseButton,
        /// The pressed modifiers.
        modifiers: Modifiers,
        /// A flag if the key is clicked, not released.
        down: bool,
    },
    /// The event when the mouse is moved.
    MouseMove {
        /// The new horizonal posiiton of the mouse.
        x: f32,
        /// The new vertical posiiton of the mouse.
        y: f32,
    },
    /// The event when the mouse wheel is rotated.
    MouseWheel(f32),
    /// The event for files dragged or dropped over the window.
    File {
        /// A flag, if the file is dropped, not just hovered.
        dropped: bool,
        /// The path of the file.
        path: std::path::PathBuf,
    },
    /// The quit event.
    Quit,
}

/// The input system
pub struct Input {
    events_loop: EventsLoop,
}

impl Input {
    /// Creates a new input window of specified size
    pub fn new(name: &str, w: u32, h: u32) -> (Self, GlWindow) {
        let events_loop = glutin::EventsLoop::new();
        let window = glutin::WindowBuilder::new()
            .with_title(name)
            .with_dimensions(w, h);
        let context = glutin::ContextBuilder::new()
            .with_vsync(true)
            .with_multisampling(4)
            .with_srgb(false);
        let gl_window = GlWindow::new(window, context, &events_loop).unwrap();

        unsafe {
            gl_window.make_current().unwrap();
            gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        }

        (Self { events_loop }, gl_window)
    }
}

use crate::engine::{Handler, System};

impl<H: Handler> System<H> for Input
where
    H::Event: From<InputEvent>,
{
    fn process(&mut self, handler: &mut H) {
        let Self { events_loop } = self;
        events_loop.poll_events(|event| {
            if let Some(event) = match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::CloseRequested => Some(InputEvent::Quit),
                    glutin::WindowEvent::Resized(width, height) => {
                        Some(InputEvent::Resize { width, height })
                    }
                    glutin::WindowEvent::KeyboardInput {
                        input:
                            glutin::KeyboardInput {
                                state,
                                virtual_keycode: Some(keycode),
                                modifiers,
                                ..
                            },
                        ..
                    } => {
                        use glutin::ElementState::*;
                        let down = match state {
                            Pressed => true,
                            Released => false,
                        };
                        Some(InputEvent::Key {
                            keycode,
                            down,
                            modifiers,
                        })
                    }

                    glutin::WindowEvent::ReceivedCharacter(char) => {
                        Some(InputEvent::TextInput(char))
                    }

                    glutin::WindowEvent::DroppedFile(path) => Some(InputEvent::File {
                        dropped: true,
                        path,
                    }),

                    glutin::WindowEvent::HoveredFile(path) => Some(InputEvent::File {
                        dropped: false,
                        path,
                    }),

                    glutin::WindowEvent::MouseInput {
                        state,
                        button,
                        modifiers,
                        ..
                    } => {
                        use glutin::ElementState::*;
                        let down = match state {
                            Pressed => true,
                            Released => false,
                        };
                        Some(InputEvent::MouseClick {
                            button,
                            down,
                            modifiers,
                        })
                    }
                    glutin::WindowEvent::MouseWheel { delta, .. } => {
                        use glutin::MouseScrollDelta::*;
                        let delta = match delta {
                            LineDelta(_, delta) => delta,
                            PixelDelta(_, delta) => delta,
                        };
                        Some(InputEvent::MouseWheel(delta))
                    }
                    glutin::WindowEvent::CursorMoved { position, .. } => {
                        Some(InputEvent::MouseMove {
                            x: position.0 as f32,
                            y: position.1 as f32,
                        })
                    }
                    _ => None,
                },
                _ => None,
            } {
                handler.handle(event.into());
            }
        });
    }
}
